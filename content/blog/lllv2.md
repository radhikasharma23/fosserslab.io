+++
title = "Let’s Learn Linux-V2"
image ="/img/llv2.jpeg"
date = "2018-10-03T21:49:20+02:00"
tags = ["CSE", "IEDC","Linux","Logo Release"]
categories = ["Workshops"]
author = "Vishnu K"
+++

## FOSSers Club organises one-day workshop on “Let’s Learn Linux-V2” 

![Image](/img/llv2.jpeg)

FOSSers,in association with IEDC,conducted a one-day fully hands on session titled “Lets Learn Linux – V2 ” on 2 October 2018 at ME CAD Lab. As many as 35 students of S1 and S3 B Tech batches participated in the event. The activities relating to installation and basic command line operations were handled by Mr Shali K R (Server Administrator). Mr Subin (S3 B Tech CSE) gave an introductions to FOSS and free software alternatives.

During the event, a new logo of FOSSers Club, designed by _Akshay T G (S3 B Tech ME)_ was released.

![FOSSers Logo](/img/blogo.png)
