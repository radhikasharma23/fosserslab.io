+++
title = "Debian Packaging Workshop"
image = ""
date = "2019-08-20T00:00:00+05:30"
tags = ["stall"]
categories = ["events"]
author = "Nitya Prakash"
aliases = ["/blog/debianpackagingworkshop"]
+++

## **[REGISTER HERE !](https://ee.kobotoolbox.org/single/::CGbHbkeo)**

Hi everyone !

We're happy to announce that we're conducting a Debian Packaging workshop at Vidya on **24th August, 2019** starting at **9AM**. We're also doing a **Debian 10 Buster release party** on the same day. 

This is a public event and anyone can join free.

**IMPORTANT : Please [read the prerequisites](https://www.loomio.org/d/LTpSdMuX/debian-packaging-pre-requisites) and setup your environment before attending the workshop.**

## Speakers

Two Debian developers will be joining the event and handling the sessions !

* Pirate Praveen
* Sruthi

> Event On : 2019 August 24th, 9AM
> 
> Event At : [Language Lab(computer centre), Decennial Block (OpenStreetMap Link)](https://www.openstreetmap.org/node/6310148494)

[How to reach Vidya](/contact)

From Thrissur,
* Get into a local private bus to Kunnamkulam (You can easily hop in one near the railway station itself).
* Get off at the bus stop named "Kaipparambu" `കൈപ്പറമ്പ്`
* It's 2kms from there to college. Use an auto (60Rs max)

From Kunnamkulam
* Get into a local private bus to Thrissur
* Get off at the bus stop named "Kaipparambu" `കൈപ്പറമ്പ്`
* It's 2kms from there to college. Use an auto (60Rs max)

From Kozhikode,
* Reach Kunnamkulam via KSRTC or private bus


## What's Debian Packaging ?

This is how we install a software in Debian/Ubuntu/Linux Mint etc :

```
sudo apt install gcompris
```

That "gcompris" is a package. In this workshop, we'll learn how to make such packages and upload to the main Debian repository which will make your package available in many operating systems such as Ubuntu, Mint, KDE Neon etc.

By making & uploading a package, you'll make a significant contribution to the Debian project.

To know more, feel free to ask in the FOSSers telegram group/matrix. [Contact](http://fossers.vidyaacademy.ac.in/contact/)

## About Debian

Debian is a free operating system (OS) for computers. Debian provides more than a pure OS. It comes with over 59000 packages, precompiled software bundled up in a nice format for easy installation on your machine.

Debian is the parent base of Ubuntu, Linux Mint and countless other operating systems. A contribution to Debian impacts millions of users around the world.

## About Debian Packaging
A package manager or package-management system is a collection of software tools that automates the process of installing, upgrading, configuring, and removing computer programs for a computer's operating system in a consistent manner.

A package manager deals with packages, distributions of software and data in archive files. Packages contain metadata, such as the software's name, description of its purpose, version number, vendor, checksum, and a list of dependencies necessary for the software to run properly. They work closely with software repositories, binary repository managers, and app stores.

## About The Program

Through this advanced workshop, we plan to introduce students to packaging and make them package maintainers thereby making a significant contribution to The Debian Project.

At the end of the workshop, the participants will learn to make a package and add to the official debian repository.

## **[REGISTER HERE !](https://ee.kobotoolbox.org/single/::CGbHbkeo)**

![Poster](/img/blog/debian-packaging-workshop-poster1.png)
